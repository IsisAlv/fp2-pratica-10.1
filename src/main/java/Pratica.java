

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Isis
 */
public class Pratica {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        System.out.println("SO: "+System.getProperty("os.name"));
        System.out.println("N. de processadores: "+rt.availableProcessors());
        System.out.println("Memoria total: "+rt.totalMemory()/1048576+" MB");
        System.out.println("Memoria livre: "+rt.freeMemory()/1048576+" MB");
        System.out.println("Max. memoria usada: "+rt.maxMemory()/1048576+" MB");
    }
}
